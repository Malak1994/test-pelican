var masthead;

function throttle(callback, wait) {  
    var time,
    go = true;
    return function() {
        if(go) {
            go = false;
            time = setTimeout(function(){
                time = null;
                go = true;
                callback.call();
            }, wait);
        }
    }
}

masthead = document.getElementById('masthead');
navbar = document.getElementById('nav');

scrollHandler = throttle(function(){  
    if(window.scrollY >= masthead.scrollHeight){
        navbar.className = "reveal";
    } else {
        navbar.className = "fixed-top";
    }
}, 250);

window.onscroll = scrollHandler;
